package com.example.igor.news.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.data.service.NewsWorker
import com.example.igor.news.ui.screens.splash.SplashActivityInterface
import com.example.igor.news.utils.PreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

@InjectViewState
class SplashActivityPresenter(private val mRepository: RepositoryInterface,
                              private val mPreferencesUtils: PreferencesUtils) :
        MvpPresenter<SplashActivityInterface>(), LifecycleObserver {

    private val mCompositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        onDataLoaded()
    }

    private fun onDataLoaded() {
        if (mPreferencesUtils.getFirstLaunch()) {
            mCompositeDisposable.add(mRepository.loadNews()
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { viewState.showLoading() }
                    .subscribe({
                        mPreferencesUtils.saveFirstLaunch(false)
                        onShowMainActivity()

                    }, {
                        viewState.hideLoading()
                    }))

            onStartUpdateWorker()

        } else {
            onShowMainActivity()
        }
    }

    private fun onStartUpdateWorker() {
        NewsWorker.start()
    }

    private fun onShowMainActivity() {
        viewState.showMainActivity()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onActivityDestroy() {
        if (!mCompositeDisposable.isDisposed) {
            mCompositeDisposable.dispose()
        }
    }
}