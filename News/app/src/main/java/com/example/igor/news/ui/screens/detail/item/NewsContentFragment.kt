package com.example.igor.news.ui.screens.detail.item

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.*
import android.webkit.*
import butterknife.ButterKnife
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.igor.news.R
import com.example.igor.news.application.NewsApplication
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.data.view.model.NewsViewModel
import javax.inject.Inject
import android.widget.LinearLayout
import butterknife.BindView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.example.igor.news.presenters.NewsContentFragmentPresenter


class NewsContentFragment : MvpAppCompatFragment(), NewsContentFragmentInterface {

    @InjectPresenter
    lateinit var mPresenter: NewsContentFragmentPresenter
    @Inject
    lateinit var mRepository: RepositoryInterface
    @BindView(R.id.webView)
    lateinit var webView: WebView
    @BindView(R.id.loadingView)
    lateinit var loadingView: ConstraintLayout
    @BindView(R.id.webViewError)
    lateinit var webViewError: LinearLayout
    private lateinit var mFavoriteMenuItem: MenuItem

    @ProvidePresenter
    fun provideNewsContentFragmentPresenter(): NewsContentFragmentPresenter {
        return NewsContentFragmentPresenter(mRepository)
    }

    init {
        NewsApplication.appComponent.inject(this)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_news_content, container, false)
        (activity as AppCompatActivity).supportActionBar!!.title = resources
                .getString(R.string.title_fragment_news_content)
        setHasOptionsMenu(true)
        ButterKnife.bind(this, view)
        lifecycle.addObserver(mPresenter)
        setupWebView()
        mPresenter.onViewSetup(arguments?.getParcelable(NEWS_CONTENT))
        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.news_content_fragment_menu, menu)
        mFavoriteMenuItem = menu.findItem(R.id.favorite)
        mPresenter.onMenuCreated()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.reload -> mPresenter.onNewsReloaded()
            R.id.favorite -> mPresenter.onLikeButtonClick()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showNews(news: NewsViewModel?) {
        news?.let {
            webView.loadUrl(news.link)
        }
    }

    override fun updateLikeButton(isLike: Boolean) {

        activity?.let {
            when {
                isLike -> mFavoriteMenuItem.icon = ContextCompat.getDrawable(it, R.drawable.ic_like)
                else -> mFavoriteMenuItem.icon = ContextCompat.getDrawable(it, R.drawable.ic_unlike)
            }
        }
    }

    override fun showLoading() {
        loadingView.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        webViewError.visibility = View.GONE
        loadingView.visibility = View.GONE
    }

    override fun hideLoadingWithError() {
        loadingView.visibility = View.GONE
        webViewError.visibility = View.VISIBLE
    }

    private fun setupWebView() {
        webView.webViewClient = NewsWebViewClient()
        webView.settings.javaScriptEnabled = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.setAppCacheEnabled(true)
        webView.settings.setAppCachePath(activity?.cacheDir?.path)
        webView.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
    }

    private inner class NewsWebViewClient : WebViewClient() {

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            mPresenter.onStartLoading()
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            mPresenter.onFinishLoading()
        }

        @Override
        override fun onReceivedError(view: WebView,
                                     request: WebResourceRequest,
                                     error: WebResourceError) {

            mPresenter.onLoadingErrorOccurred()
        }
    }

    companion object {

        private const val NEWS_CONTENT = "NEWS_CONTENT"
        fun newInstance(model: NewsViewModel): NewsContentFragment {

            val fragment = NewsContentFragment()
            val bundle = Bundle()
            bundle.putParcelable(NEWS_CONTENT, model)
            fragment.arguments = bundle
            return fragment
        }
    }
}