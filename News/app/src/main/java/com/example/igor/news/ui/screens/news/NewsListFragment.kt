package com.example.igor.news.ui.screens.news

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.igor.news.ui.adapters.NewsRecyclerViewAdapter
import com.example.igor.news.R
import com.example.igor.news.application.NewsApplication
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.presenters.NewsListFragmentPresenter
import com.example.igor.news.ui.enum.ScreenType
import com.example.igor.news.ui.screens.detail.container.NewsContentContainerActivity
import javax.inject.Inject

class NewsListFragment : MvpAppCompatFragment(),
        NewsListFragmentInterface {

    @InjectPresenter
    lateinit var mPresenter: NewsListFragmentPresenter
    @Inject
    lateinit var mRepository: RepositoryInterface
    @BindView(R.id.list)
    lateinit var list: RecyclerView
    @BindView(R.id.swipeRefresh)
    lateinit var swipeRefresh: SwipeRefreshLayout

    private val mRecyclerViewClickListener = object : NewsRecyclerViewAdapter.NewsAdapterListener {
        override fun newsAdapterItemClick(itemIndex: Int) {
            activity?.let {
                NewsContentContainerActivity.launch(it, itemIndex, ScreenType.NEWS)
            }
        }
    }

    @ProvidePresenter
    fun provideNewsListFragmentPresenter(): NewsListFragmentPresenter {
        return NewsListFragmentPresenter(mRepository)
    }

    init {
        NewsApplication.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_news_list, container, false)
        lifecycle.addObserver(mPresenter)
        ButterKnife.bind(this, view)
        list.layoutManager = LinearLayoutManager(context)
        swipeRefresh.setOnRefreshListener {
            mPresenter.onNewsLoaded()
        }

        return view
    }

    override fun hideLoading() {
        swipeRefresh.isRefreshing = false
    }

    override fun showLoading() {}

    override fun showNews(items: List<NewsViewModel>) {
        list.adapter = NewsRecyclerViewAdapter(items, mRecyclerViewClickListener)
    }
}
