package com.example.igor.news.di.module

import android.arch.persistence.room.Room
import android.content.Context
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.data.Repository
import com.example.igor.news.data.db.helper.DbHelper
import com.example.igor.news.data.db.DbInterface
import com.example.igor.news.data.db.helper.RoomDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideDataBase(context: Context): RoomDb {
        return Room.databaseBuilder(context,
                RoomDb::class.java, "database")
                .build()
    }

    @Provides
    @Singleton
    fun provideDbHelper(database: RoomDb): DbInterface {
        return DbHelper(database)
    }

    @Singleton
    @Provides
    fun provideDataService() : RepositoryInterface {
        return Repository.instance
    }
}