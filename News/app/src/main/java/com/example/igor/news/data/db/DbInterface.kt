package com.example.igor.news.data.db

import com.example.igor.news.data.db.entities.NewsEntity
import com.example.igor.news.data.view.model.NewsViewModel
import io.reactivex.Flowable
import io.reactivex.Single

interface DbInterface {

    fun saveNews(newsEntities: List<NewsEntity>)
    fun fetchNewsFromBd() : Flowable<List<NewsViewModel>>
    fun singleSelectNewsFromBd(): Single<List<NewsViewModel>>
    fun addToFavorite(entityId: Long) : Single<Unit>
    fun removeFromFavorite(entityId: Long): Single<Unit>
    fun fetchFavoriteNews() : Single<List<NewsViewModel>>
}