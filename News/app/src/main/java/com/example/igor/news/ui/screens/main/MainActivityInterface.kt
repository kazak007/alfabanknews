package com.example.igor.news.ui.screens.main

import com.arellomobile.mvp.MvpView

interface MainActivityInterface : MvpView {

    fun showPage(page: Int)
}