package com.example.igor.news.utils

import com.example.igor.news.data.server.rxoperator.error.ApiErrorOperator

object RxUtils {

    fun <T> getApiErrorTransformer(): ApiErrorOperator<T> {

        return ApiErrorOperator()
    }
}