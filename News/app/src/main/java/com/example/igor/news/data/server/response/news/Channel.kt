package com.example.igor.news.data.server.response.news

import org.simpleframework.xml.Root
import org.simpleframework.xml.ElementList
import java.io.Serializable

@Root(name = "channel", strict = false)
data class Channel @JvmOverloads constructor(
        @field:ElementList(inline = true, name = "item")
        var items: List<NewsItem>? = null)