package com.example.igor.news.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.example.igor.news.R
import com.example.igor.news.data.view.model.NewsViewModel
import kotlinx.android.synthetic.main.fragment_favorite.view.*

class FavoriteRecyclerViewAdapter(
        private val mValues: List<NewsViewModel>,
        private val mListener: FavoriteAdapterListener?)
    : RecyclerView.Adapter<FavoriteRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    private val mOnDeleteListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val index = v.tag as Int
            mListener?.favoriteAdapterItemClick(index)
        }

        mOnDeleteListener = View.OnClickListener { v ->
            val item = v.tag as NewsViewModel
            mListener?.itemDeleteClick(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_favorite, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.titleTextView.text = item.title

        with(holder.mView) {
            tag = position
            setOnClickListener(mOnClickListener)
        }

        with(holder.buttonDelete) {
            tag = item
            setOnClickListener(mOnDeleteListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val titleTextView: TextView = mView.title
        val buttonDelete: ImageButton = mView.buttonDelete
    }

    interface FavoriteAdapterListener {

        fun favoriteAdapterItemClick(itemIndex: Int)
        fun itemDeleteClick(item: NewsViewModel?)
    }
}
