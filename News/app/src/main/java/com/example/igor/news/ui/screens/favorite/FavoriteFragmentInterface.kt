package com.example.igor.news.ui.screens.favorite

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.ui.progress.LoadingInterface

interface FavoriteFragmentInterface : MvpView {

    @StateStrategyType(value = SingleStateStrategy::class)
    fun showNews(items: List<NewsViewModel>)
}