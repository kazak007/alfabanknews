package com.example.igor.news.data.server.response.news

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root
import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Serializer
import java.io.Serializable

@Root(name = "rss", strict = false)
data class NewsResponse @JvmOverloads constructor(
        @field:Element(name = "channel")
        var channel: Channel? = null)