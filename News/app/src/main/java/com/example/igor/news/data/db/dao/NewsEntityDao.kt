package com.example.igor.news.data.db.dao

import android.arch.persistence.room.*
import com.example.igor.news.data.db.entities.NewsEntity
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
abstract class NewsEntityDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(newsEntities: List<NewsEntity>)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    abstract fun update(newsEntities: List<NewsEntity>)

    @Transaction
    open fun insertOrUpdate(newsEntities: List<NewsEntity>) {
        insert(newsEntities)
        update(newsEntities)
    }

    @Delete
    abstract fun delete(newsEntities: NewsEntity)

    @Query("SELECT * FROM NewsEntity")
    abstract fun getAll(): Flowable<List<NewsEntity>>

    @Query("SELECT * FROM NewsEntity")
    abstract fun singleSelectNewsFromBd(): Single<List<NewsEntity>>

    @Query("SELECT * FROM NewsEntity WHERE ${NewsEntity.FAVORITE} = 1")
    abstract fun getFavoriteNews(): Single<List<NewsEntity>>

    fun addToFavorite(entityId: Long) {
        return setFavorite(true, entityId)
    }

    fun removeFromFavorite(entityId: Long) {
        return setFavorite(false, entityId)
    }

    @Query("UPDATE NewsEntity set ${NewsEntity.FAVORITE} = :value WHERE ${NewsEntity.ID} = :entityId")
    abstract fun setFavorite(value: Boolean, entityId: Long)

}