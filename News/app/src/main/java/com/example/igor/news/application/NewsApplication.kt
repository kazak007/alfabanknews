package com.example.igor.news.application

import android.app.Application
import com.example.igor.news.di.component.ApplicationComponent
import com.example.igor.news.di.component.DaggerApplicationComponent
import com.example.igor.news.di.module.ApplicationModule

class NewsApplication : Application() {

    companion object {
        @JvmStatic lateinit var appComponent: ApplicationComponent
    }

    @Suppress("DEPRECATION")
    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }
}