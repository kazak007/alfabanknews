package com.example.igor.news.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.igor.news.ui.screens.main.MainActivityInterface

@InjectViewState
class MainActivityPresenter : MvpPresenter<MainActivityInterface>() {

    private var mCurrentPage: Int = 0

    fun onRestoreSelectedPage() {
        viewState.showPage(mCurrentPage)
    }

    fun onSaveCurrentPosition(position : Int) {
        mCurrentPage = position
    }
}