package com.example.igor.news.data

import com.example.igor.news.data.db.DbInterface
import com.example.igor.news.data.view.model.NewsViewModel
import io.reactivex.Single

interface RepositoryInterface : DbInterface {

    fun loadNews() : Single<List<NewsViewModel>>
}