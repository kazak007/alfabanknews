package com.example.igor.news.data.server.response.news

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "item", strict = false)
data class NewsItem @JvmOverloads constructor(

        @field:Element(name = "title")
        var title: String = "",
        @field:Element(name = "link")
        var link: String = "",
        @field:Element(name = "description")
        var description: String = "")