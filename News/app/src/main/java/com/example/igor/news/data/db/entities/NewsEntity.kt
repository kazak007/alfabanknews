package com.example.igor.news.data.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.example.igor.news.data.server.response.news.NewsItem

@Entity(tableName = "NewsEntity")
open class NewsEntity(
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = "title")
        var title: String = "",
        @ColumnInfo(name = "link")
        var link: String = "",
        @ColumnInfo(name = "description")
        var description: String = "",
        @ColumnInfo(name = "favorite")
        var favorite: Boolean = false) {

    @Ignore
    constructor() : this(0, "", "", "", false)

    @Ignore
    constructor(model: NewsItem) : this() {
        title = model.title
        link = model.link
        description = model.description
    }

    companion object {

        const val ID = "id"
        const val TITLE = "title"
        const val LINK = "link"
        const val DESCRIPTION = "description"
        const val FAVORITE = "favorite"
    }
}

