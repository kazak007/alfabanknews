package com.example.igor.news.ui.enum

enum class ScreenType {
    NEWS, FAVORITE
}