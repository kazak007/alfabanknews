package com.example.igor.news.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.igor.news.R
import com.example.igor.news.data.view.model.NewsViewModel

import kotlinx.android.synthetic.main.fragment_news.view.*

class NewsRecyclerViewAdapter(
        private val mValues: List<NewsViewModel>,
        private val mListener: NewsAdapterListener?)
    : RecyclerView.Adapter<NewsRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val index = v.tag as Int
            mListener?.newsAdapterItemClick(index)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_news, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.titleTextView.text = item.title
        holder.descriptionTextView.text = item.description

        with(holder.mView) {
            tag = position
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val titleTextView: TextView = mView.title
        val descriptionTextView: TextView = mView.description
    }

    interface NewsAdapterListener {
        fun newsAdapterItemClick(itemIndex: Int)
    }
}
