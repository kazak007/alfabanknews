package com.example.igor.news.ui.screens.detail.container

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.igor.news.data.view.model.NewsViewModel

interface NewsContentContainerInterface : MvpView {

    @StateStrategyType(value = SingleStateStrategy::class)
    fun setupViewPager(items: List<NewsViewModel>)
}