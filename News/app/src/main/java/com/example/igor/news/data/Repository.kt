package com.example.igor.news.data


import com.example.igor.news.BuildConfig
import com.example.igor.news.application.NewsApplication
import com.example.igor.news.data.db.entities.NewsEntity
import com.example.igor.news.data.db.DbInterface
import com.example.igor.news.data.server.Api
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.utils.RxUtils
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import javax.inject.Inject
import kotlin.jvm.java

class Repository private constructor() : RepositoryInterface {


    @Inject
    lateinit var dbHelper: DbInterface

    private object HOLDER {
        val INSTANCE = Repository()
    }

    companion object {
        val instance: Repository by lazy { HOLDER.INSTANCE }
    }

    init {
        NewsApplication.appComponent.inject(this)
    }

    @Suppress("DEPRECATION")
    private val service: Api by lazy {
        Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict(
                        Persister(AnnotationStrategy())))
                .baseUrl(BuildConfig.HOST)
                .build()
                .create(Api::class.java)
    }

    override fun loadNews() : Single<List<NewsViewModel>> {
        return service.news()
                .subscribeOn(Schedulers.io())
                .lift(RxUtils.getApiErrorTransformer())
                .flatMap { newsResponse ->
                    val entities = mutableListOf<NewsEntity>()
                    newsResponse.channel?.items?.forEach { newsItem ->
                        entities.add(NewsEntity(newsItem))
                    }
                    saveNews(entities)
                    Single.just(newsResponse)
                }
                .flatMap {
                    val models = mutableListOf<NewsViewModel>()
                    it.channel?.items?.forEach {
                        models.add(NewsViewModel(it))
                    }
                    Single.just(models)
                }
    }

    override fun singleSelectNewsFromBd(): Single<List<NewsViewModel>> {
        return dbHelper.singleSelectNewsFromBd()
    }

    override fun saveNews(newsEntities: List<NewsEntity>) {
        dbHelper.saveNews(newsEntities)
    }

    override fun fetchFavoriteNews() : Single<List<NewsViewModel>> {
        return dbHelper.fetchFavoriteNews()
    }

    override fun fetchNewsFromBd() : Flowable<List<NewsViewModel>> {
        return dbHelper.fetchNewsFromBd()
    }

     override fun addToFavorite(entityId: Long) : Single<Unit> {
        return dbHelper.addToFavorite(entityId)
    }

     override fun removeFromFavorite(entityId: Long) : Single<Unit> {
        return dbHelper.removeFromFavorite(entityId)
    }
}