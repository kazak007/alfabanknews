package com.example.igor.news.di.module

import android.content.Context
import com.example.igor.news.application.NewsApplication
import com.example.igor.news.utils.PreferencesUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val mApplication: NewsApplication) {

    @Singleton
    @Provides
    fun provideApplication() : NewsApplication {
        return mApplication
    }

    @Singleton
    @Provides
    fun provideContext() : Context {
        return mApplication
    }

    @Singleton
    @Provides
    fun providePreferencesUtils(context: Context) : PreferencesUtils {
        return PreferencesUtils(context)
    }
}