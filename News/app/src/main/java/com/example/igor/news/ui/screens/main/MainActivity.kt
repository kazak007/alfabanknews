package com.example.igor.news.ui.screens.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.igor.news.R
import com.example.igor.news.presenters.MainActivityPresenter
import com.example.igor.news.ui.adapters.MainActivityPagerAdapter

class MainActivity : MvpAppCompatActivity(), MainActivityInterface {

    @InjectPresenter
    lateinit var mPresenter: MainActivityPresenter
    @BindView(R.id.viewPager)
    lateinit var viewPager: ViewPager
    @BindView(R.id.tabLayout)
    lateinit var tabLayout: TabLayout
    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    @ProvidePresenter
    fun provideMainActivityPresenter(): MainActivityPresenter {
        return MainActivityPresenter()
    }

    private var mSectionsPagerAdapter: MainActivityPagerAdapter? = null
    private val mPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {}
        override fun onPageScrolled(position: Int,
                                    positionOffset: Float,
                                    positionOffsetPixels: Int) {}
        override fun onPageSelected(position: Int) {
            mPresenter.onSaveCurrentPosition(position)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = resources.getString(R.string.title_activity_main)
        initViewPager()
    }

    private fun initViewPager() {
        val list = mutableListOf<String>()
        list.add(resources.getString(R.string.title_fragment_news_list))
        list.add(resources.getString(R.string.title_fragment_favorite))

        mSectionsPagerAdapter = MainActivityPagerAdapter(supportFragmentManager, list)
        viewPager.adapter = mSectionsPagerAdapter
        tabLayout.setupWithViewPager(viewPager)
        viewPager.addOnPageChangeListener(mPageChangeListener)
    }

    override fun onResume() {
        super.onResume()
        mPresenter.onRestoreSelectedPage()
    }

    override fun showPage(page: Int) {
        viewPager.currentItem = page
    }

    companion object {

        fun launch(parent: Activity) {
            val i = Intent(parent, MainActivity::class.java)
            parent.startActivity(i)
        }
    }
}