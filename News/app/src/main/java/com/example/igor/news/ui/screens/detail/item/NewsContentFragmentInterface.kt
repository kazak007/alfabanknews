package com.example.igor.news.ui.screens.detail.item

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.ui.progress.LoadingInterface

interface NewsContentFragmentInterface : MvpView, LoadingInterface {

    @StateStrategyType(value = SingleStateStrategy::class)
    fun showNews(news: NewsViewModel?)
    @StateStrategyType(value = SkipStrategy::class)
    fun updateLikeButton(isLike: Boolean)
    @StateStrategyType(value = SkipStrategy::class)
    fun hideLoadingWithError()
}