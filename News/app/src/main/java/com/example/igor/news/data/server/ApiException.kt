package com.example.igor.news.data.server

data class ApiException(val mMessage: String, val code: Int) : Exception()

