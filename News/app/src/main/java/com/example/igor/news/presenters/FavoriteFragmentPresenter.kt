package com.example.igor.news.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.ui.screens.favorite.FavoriteFragmentInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

@InjectViewState
class FavoriteFragmentPresenter(private val mRepository: RepositoryInterface) :
        MvpPresenter<FavoriteFragmentInterface>(), LifecycleObserver {

    private val mCompositeDisposable = CompositeDisposable()
    private var mNewsList = mutableListOf<NewsViewModel>()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        fetchFavorites()
    }

    private fun fetchFavorites() {
        val disposable = mRepository.fetchFavoriteNews()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mNewsList.addAll(it)
                    viewState.showNews(it)
                }, {
                    it.printStackTrace()
                })

        mCompositeDisposable.addAll(disposable)
    }

    fun onRemoveFromFavorities(item: NewsViewModel) {

        item.id?.let {
            val disposable = mRepository.removeFromFavorite(it)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        mNewsList.remove(item)
                        viewState.showNews(mNewsList)
                    }, {
                        it.printStackTrace()
                    })

            mCompositeDisposable.addAll(disposable)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onActivityDestroy() {
        if (!mCompositeDisposable.isDisposed) {
            mCompositeDisposable.dispose()
        }
    }
}