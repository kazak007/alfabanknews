package com.example.igor.news.utils

object CastTypeExtension {

    fun Boolean.toInt() = if (this) 1 else 0
    fun Int.toBoolean() = this == 1
}