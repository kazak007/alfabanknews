package com.example.igor.news.ui.screens.detail.container

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import butterknife.BindView
import butterknife.ButterKnife
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter

import com.example.igor.news.R
import com.example.igor.news.application.NewsApplication
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.presenters.NewsContentContainerPresenter
import com.example.igor.news.ui.adapters.NewsContentPagerAdapter
import com.example.igor.news.ui.enum.ScreenType
import javax.inject.Inject

class NewsContentContainerActivity : MvpAppCompatActivity(), NewsContentContainerInterface {

    @InjectPresenter
    lateinit var mPresenter: NewsContentContainerPresenter
    @Inject
    lateinit var mRepository: RepositoryInterface
    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar
    @BindView(R.id.viewPager)
    lateinit var viewPager: ViewPager
    private var mPagerAdapter: NewsContentPagerAdapter? = null

    private val mPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {}
        override fun onPageScrolled(position: Int,
                                    positionOffset: Float,
                                    positionOffsetPixels: Int) {}
        override fun onPageSelected(position: Int) {
            mPresenter.onSaveCurrentPosition(position)
        }
    }

    @ProvidePresenter
    fun provideNewsContentActivityPresenter(): NewsContentContainerPresenter {
        val screenType = intent.getSerializableExtra(SCREEN_TYPE) as ScreenType
        return NewsContentContainerPresenter(mRepository, screenType)
    }

    init {
        NewsApplication.appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_content_container)
        lifecycle.addObserver(mPresenter)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewPager.addOnPageChangeListener(mPageChangeListener)
    }

    override fun setupViewPager(items: List<NewsViewModel>) {
        val index = intent.getIntExtra(NEWS_INDEX, 0)
        mPagerAdapter = NewsContentPagerAdapter(items, supportFragmentManager)
        viewPager.adapter = mPagerAdapter
        viewPager.currentItem = index
    }

    companion object {

        private const val NEWS_INDEX = "NEWS_INDEX"
        private const val SCREEN_TYPE = "SCREEN_TYPE"

        fun launch(parent: Activity,
                   itemIndex: Int,
                   screenType: ScreenType) {

            val i = Intent(parent, NewsContentContainerActivity::class.java)
            i.putExtra(NEWS_INDEX, itemIndex)
            i.putExtra(SCREEN_TYPE, screenType)
            parent.startActivity(i)
        }
    }
}
