package com.example.igor.news.ui.screens.splash

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.igor.news.ui.progress.LoadingInterface

@StateStrategyType(value = SkipStrategy::class)
interface SplashActivityInterface : MvpView, LoadingInterface {

    fun showMainActivity()
}