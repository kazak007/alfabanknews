package com.example.igor.news.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.ui.screens.detail.item.NewsContentFragmentInterface
import io.reactivex.disposables.CompositeDisposable


@InjectViewState
class NewsContentFragmentPresenter(private val mRepository: RepositoryInterface) :
        MvpPresenter<NewsContentFragmentInterface>(), LifecycleObserver {

    private var mNewsModel: NewsViewModel? = null
    private val mCompositeDisposable = CompositeDisposable()
    private var mIsErrorOccurred: Boolean = false

    fun onViewSetup(model: NewsViewModel?) {
        model?.let {
            mNewsModel = model
            viewState.showNews(model)
            viewState.updateLikeButton(model.favorite)
        }
    }

    fun onNewsReloaded() {
        viewState.showNews(mNewsModel)
    }

    fun onLikeButtonClick() {
        mNewsModel?.let { model ->

            if (model.favorite) {
                mCompositeDisposable.add(mRepository.removeFromFavorite(model.id)
                        .subscribe({
                            model.favorite = false
                            viewState.updateLikeButton(false)
                        }, {
                            it.printStackTrace()
                        }))
            } else {
                mCompositeDisposable.add(mRepository.addToFavorite(model.id)
                        .subscribe({
                            model.favorite = true
                            viewState.updateLikeButton(true)
                        }, {
                            it.printStackTrace()
                        }))
            }
        }
    }

    fun onMenuCreated() {
        viewState.updateLikeButton(mNewsModel?.favorite ?: false)
    }

    fun onLoadingErrorOccurred() {
        mIsErrorOccurred = true
    }

    fun onStartLoading() {
        mIsErrorOccurred = false
        viewState.showLoading()
    }

    fun onFinishLoading() {
        mNewsModel?.favorite?.let {
            if (!it && mIsErrorOccurred) {
                viewState.hideLoadingWithError()
            } else {
                viewState.hideLoading()
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onActivityDestroy() {
        if (!mCompositeDisposable.isDisposed) {
            mCompositeDisposable.dispose()
        }
    }
}