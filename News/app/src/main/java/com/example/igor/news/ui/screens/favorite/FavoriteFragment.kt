package com.example.igor.news.ui.screens.favorite

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.igor.news.R
import com.example.igor.news.application.NewsApplication
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.presenters.FavoriteFragmentPresenter
import com.example.igor.news.ui.adapters.FavoriteRecyclerViewAdapter
import com.example.igor.news.ui.enum.ScreenType
import com.example.igor.news.ui.screens.detail.container.NewsContentContainerActivity
import javax.inject.Inject

class FavoriteFragment : MvpAppCompatFragment(), FavoriteFragmentInterface {

    @InjectPresenter
    lateinit var mPresenter: FavoriteFragmentPresenter
    @Inject
    lateinit var mRepository: RepositoryInterface
    @BindView(R.id.list)
    lateinit var recyclerView: RecyclerView

    @ProvidePresenter
    fun provideFavoriteFragmentPresenter(): FavoriteFragmentPresenter {
        return FavoriteFragmentPresenter(mRepository)
    }

    private val mAdapterListener = object : FavoriteRecyclerViewAdapter.FavoriteAdapterListener {

        override fun itemDeleteClick(item: NewsViewModel?) {
            item?.let {
                mPresenter.onRemoveFromFavorities(item)
            }
        }

        override fun favoriteAdapterItemClick(itemIndex: Int) {
            activity?.let {
                NewsContentContainerActivity.launch(it, itemIndex, ScreenType.FAVORITE)
            }
        }
    }

    init {
        NewsApplication.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favorite_list, container, false)
        lifecycle.addObserver(mPresenter)
        ButterKnife.bind(this, view)
        recyclerView.layoutManager = LinearLayoutManager(context)
        return view
    }

    override fun showNews(items: List<NewsViewModel>) {
        recyclerView.adapter = FavoriteRecyclerViewAdapter(items, mAdapterListener)
    }
}
