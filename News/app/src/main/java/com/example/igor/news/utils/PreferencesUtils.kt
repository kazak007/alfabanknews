package com.example.igor.news.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class PreferencesUtils(context: Context) {

    private val FIRST_LAUNCH = "FIRST_LAUNCH"
    private var mSharedPreferences: SharedPreferences? = null

    init {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun saveFirstLaunch(value: Boolean) {
        setValue(FIRST_LAUNCH, value)
    }

    fun getFirstLaunch(): Boolean {
        return getBooleanValue(FIRST_LAUNCH, true)
    }

    private fun setValue(key: String, value: Boolean) {
        val ed = mSharedPreferences?.edit()
        ed?.putBoolean(key, value)
        ed?.apply()
    }

    private fun getBooleanValue(key: String, defValue: Boolean): Boolean {
        return mSharedPreferences?.getBoolean(key, defValue) ?: false
    }
}