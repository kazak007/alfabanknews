package com.example.igor.news.data.db.helper

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.igor.news.data.db.dao.NewsEntityDao
import com.example.igor.news.data.db.entities.NewsEntity


@Database(entities = [NewsEntity::class],
        version = 7, exportSchema = false)
abstract class RoomDb : RoomDatabase() {

    abstract fun newsItemDao(): NewsEntityDao
}