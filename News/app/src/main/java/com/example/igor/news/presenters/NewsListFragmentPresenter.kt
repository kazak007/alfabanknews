package com.example.igor.news.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.ui.screens.news.NewsListFragmentInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable


@InjectViewState
class NewsListFragmentPresenter(private val mRepository: RepositoryInterface) :
        MvpPresenter<NewsListFragmentInterface>(), LifecycleObserver {

    private val mCompositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        onNewsLoaded()
    }

    fun onNewsLoaded() {
        mCompositeDisposable.add(mRepository.fetchNewsFromBd()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.showNews(it)
                    viewState.hideLoading()
                }, {
                    it.printStackTrace()
                }))
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onActivityDestroy() {
        if (!mCompositeDisposable.isDisposed) {
            mCompositeDisposable.dispose()
        }
    }
}