package com.example.igor.news.data.service
import androidx.work.*
import com.example.igor.news.data.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class NewsWorker : Worker() {

    override fun doWork(): Result {
        Repository.instance.loadNews()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, { it.printStackTrace()})

        return Result.SUCCESS
    }

    companion object {

        fun start() {
            val constraints = Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.NOT_ROAMING)
                    .build()

            val myWorkRequest: PeriodicWorkRequest = PeriodicWorkRequest
                    .Builder(NewsWorker::class.java,
                            10,
                            TimeUnit.MINUTES,
                            5,
                            TimeUnit.MINUTES)

                    .setConstraints(constraints)
                    .build()

            WorkManager.getInstance().enqueue(myWorkRequest)
        }
    }
}