package com.example.igor.news.data.view.model

import android.os.Parcel
import android.os.Parcelable
import android.text.Html
import com.example.igor.news.data.db.entities.NewsEntity
import com.example.igor.news.data.server.response.news.NewsItem
import com.example.igor.news.utils.CastTypeExtension.toBoolean
import com.example.igor.news.utils.CastTypeExtension.toInt

class NewsViewModel(var id: Long = -1,
                    var title: String? = null,
                    var link: String? = null,
                    var description: String? = null) : Parcelable {

    var favorite: Boolean = false

    constructor(model: NewsItem) : this() {

        title = parseHtml(model.title)
        link = model.link
        description = parseHtml(model.description)
    }

    constructor(model: NewsEntity) : this() {

        id = model.id
        title = parseHtml(model.title)
        link = model.link
        description = parseHtml(model.description)
        favorite = model.favorite
    }

    constructor(parcel: Parcel) : this() {
        readFromParcel(parcel)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

        parcel.writeInt(favorite.toInt())
        parcel.writeLong(id)
        parcel.writeString(title)
        parcel.writeString(link)
        parcel.writeString(description)
    }

    private fun readFromParcel(parcel: Parcel) {
        favorite = parcel.readInt().toBoolean()
        id = parcel.readLong()
        title = parcel.readString()
        link = parcel.readString()
        description = parcel.readString()
    }

    override fun describeContents(): Int {
        return 0
    }

    private fun parseHtml(htmlString: String) : String {
        return  if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            Html.fromHtml(htmlString,Html.FROM_HTML_MODE_LEGACY).toString()
        } else {
            Html.fromHtml(htmlString).toString()
        }
    }

    companion object CREATOR : Parcelable.Creator<NewsViewModel> {
        override fun createFromParcel(parcel: Parcel): NewsViewModel {
            return NewsViewModel(parcel)
        }

        override fun newArray(size: Int): Array<NewsViewModel?> {
            return arrayOfNulls(size)
        }
    }
}
