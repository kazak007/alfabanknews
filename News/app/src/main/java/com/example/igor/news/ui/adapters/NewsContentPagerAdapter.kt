package com.example.igor.news.ui.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.ui.screens.detail.item.NewsContentFragment

class NewsContentPagerAdapter(private var items: List<NewsViewModel>,
                              fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return NewsContentFragment.newInstance(items[position])
    }

    override fun getCount(): Int {
        return items.size
    }
}