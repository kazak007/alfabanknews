package com.example.igor.news.ui.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.arellomobile.mvp.MvpAppCompatFragment
import com.example.igor.news.ui.screens.favorite.FavoriteFragment
import com.example.igor.news.ui.screens.news.NewsListFragment

class MainActivityPagerAdapter(fm: FragmentManager,
                               private var mTitleList: List<String>) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when(position) {
            0 -> NewsListFragment()
            1 -> FavoriteFragment()
            else -> MvpAppCompatFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position) {
            0 -> mTitleList[position]
            1 -> mTitleList[position]
            else -> ""
        }
    }
}