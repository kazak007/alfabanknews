package com.example.igor.news.di.component


import com.example.igor.news.data.Repository
import com.example.igor.news.di.module.ApplicationModule
import com.example.igor.news.di.module.RepositoryModule
import com.example.igor.news.data.service.NewsWorker
import com.example.igor.news.ui.screens.detail.container.NewsContentContainerActivity
import com.example.igor.news.ui.screens.detail.item.NewsContentFragment
import com.example.igor.news.ui.screens.favorite.FavoriteFragment
import com.example.igor.news.ui.screens.news.NewsListFragment
import com.example.igor.news.ui.screens.splash.SplashActivity

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(RepositoryModule::class),
    (ApplicationModule::class)])
interface ApplicationComponent {

    fun inject(splashActivity: SplashActivity)
    fun inject(repository: Repository)
    fun inject(newsListFragment: NewsListFragment)
    fun inject(favoriteFragment: FavoriteFragment)
    fun inject(newsContentFragment: NewsContentFragment)
    fun inject(newsContentContainerActivity: NewsContentContainerActivity)
    fun inject(newsWorker: NewsWorker)
}