package com.example.igor.news.data.server

import com.example.igor.news.data.server.response.news.NewsResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("_/rss/_rss.html?subtype=1&category=2&city=21")
    fun news(): Single<NewsResponse>
}