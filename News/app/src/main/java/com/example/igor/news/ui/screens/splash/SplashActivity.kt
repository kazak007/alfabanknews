package com.example.igor.news.ui.screens.splash

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.view.View
import butterknife.BindView
import butterknife.ButterKnife
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.example.igor.news.R
import com.example.igor.news.application.NewsApplication
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.presenters.SplashActivityPresenter
import com.example.igor.news.ui.screens.main.MainActivity
import com.example.igor.news.utils.PreferencesUtils
import javax.inject.Inject

class SplashActivity : MvpAppCompatActivity(), SplashActivityInterface {

    @InjectPresenter
    lateinit var mPresenter: SplashActivityPresenter
    @Inject
    lateinit var mPreferencesUtils: PreferencesUtils
    @Inject
    lateinit var mRepository: RepositoryInterface
    @BindView(R.id.loadingView)
    lateinit var loadingView: ConstraintLayout

    @ProvidePresenter
    fun provideSplashActivityPresenter(): SplashActivityPresenter {
        return SplashActivityPresenter(mRepository, mPreferencesUtils)
    }

    init {
        NewsApplication.appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        lifecycle.addObserver(mPresenter)
        ButterKnife.bind(this)
    }

    override fun showMainActivity() {
        MainActivity.launch(this)
    }

    override fun showLoading() {
        loadingView.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loadingView.visibility = View.GONE
    }
}
