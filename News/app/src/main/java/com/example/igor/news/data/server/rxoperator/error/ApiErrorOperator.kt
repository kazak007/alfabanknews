package com.example.igor.news.data.server.rxoperator.error

import com.example.igor.news.data.server.ApiException
import java.io.IOException
import io.reactivex.SingleObserver
import io.reactivex.SingleOperator
import io.reactivex.disposables.Disposable
import retrofit2.adapter.rxjava2.HttpException

class ApiErrorOperator<T> :
        SingleOperator<T, T> {

    @Throws(Exception::class)
    override fun apply(observer: SingleObserver<in T>): SingleObserver<in T> {
        return object : SingleObserver<T> {
            override fun onSubscribe(d: Disposable) {
                observer.onSubscribe(d)
            }

            override fun onSuccess(t: T) {
                observer.onSuccess(t)
            }

            override fun onError(e: Throwable) {
                handleError(e, observer)
            }
        }
    }

    @Suppress("DEPRECATION")
    private fun handleError(e: Throwable, observer: SingleObserver<in T>) {

        when (e) {
            is HttpException -> {

                val response = e.response()
                val exception = ApiException(response.message(),
                        response.code())

                observer.onError(exception)
            }

            is IOException -> {
                val exception = ApiException("No Network Connection", 1)
                observer.onError(exception)
            }

            else -> observer.onError(ApiException(e.localizedMessage, 2))
        }
    }
}