package com.example.igor.news.data.db.helper

import com.example.igor.news.data.db.DbInterface
import com.example.igor.news.data.db.entities.NewsEntity
import com.example.igor.news.data.view.model.NewsViewModel
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DbHelper(private val mDatabase: RoomDb) : DbInterface {

    override fun singleSelectNewsFromBd(): Single<List<NewsViewModel>> {
        return mDatabase.newsItemDao()
                .singleSelectNewsFromBd()
                .subscribeOn(Schedulers.io())
                .flatMap {
                    val models = mutableListOf<NewsViewModel>()
                    it.forEach {
                        models.add(NewsViewModel(it))
                    }
                    Single.just(models)
                }
    }

    override fun saveNews(newsEntities: List<NewsEntity>) {
        mDatabase.newsItemDao().insertOrUpdate(newsEntities)
    }

    override fun fetchFavoriteNews() : Single<List<NewsViewModel>> {
        return mDatabase.newsItemDao()
                .getFavoriteNews()
                .subscribeOn(Schedulers.io())
                .flatMap {
                    val models = mutableListOf<NewsViewModel>()
                    it.forEach {
                        models.add(NewsViewModel(it))
                    }

                    Single.just(models)
                }
    }

    override fun fetchNewsFromBd() : Flowable<List<NewsViewModel>> {
        return mDatabase.newsItemDao()
                .getAll()
                .flatMap {
                    val models = mutableListOf<NewsViewModel>()
                    it.forEach {
                        models.add(NewsViewModel(it))
                    }

                    Flowable.just(models)
                }
    }

    override fun addToFavorite(entityId: Long) : Single<Unit> {
        return Single.fromCallable {
            mDatabase.newsItemDao().addToFavorite(entityId)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun removeFromFavorite(entityId: Long) : Single<Unit> {
        return Single.fromCallable {
            mDatabase.newsItemDao().removeFromFavorite(entityId)
        }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}