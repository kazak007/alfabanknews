package com.example.igor.news.presenters

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.igor.news.data.RepositoryInterface
import com.example.igor.news.data.view.model.NewsViewModel
import com.example.igor.news.ui.enum.ScreenType
import com.example.igor.news.ui.screens.detail.container.NewsContentContainerInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

@InjectViewState
class NewsContentContainerPresenter(private val mRepository: RepositoryInterface,
                                    private var mScreenType: ScreenType) :
        MvpPresenter<NewsContentContainerInterface>(), LifecycleObserver {

    private var mNewsList: MutableList<NewsViewModel> = mutableListOf()
    private var mCurrentPosition: Int = 0
    private val mCompositeDisposable = CompositeDisposable()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadData(mScreenType)
    }

    fun onSaveCurrentPosition(position: Int) {
        mCurrentPosition = position
    }

    private fun loadData(type: ScreenType) {
        when(type) {
            ScreenType.NEWS -> fetchAllNews()
            ScreenType.FAVORITE -> fetchFavorite()
        }
    }

    private fun fetchAllNews() {
        mCompositeDisposable.add(mRepository.singleSelectNewsFromBd()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mNewsList.clear()
                    mNewsList.addAll(it)
                    viewState.setupViewPager(it)
                }, {
                    it.printStackTrace()
                }))
    }

    private fun fetchFavorite() {
        mCompositeDisposable.add(mRepository.fetchFavoriteNews()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mNewsList.addAll(it)
                    viewState.setupViewPager(it)
                }, {
                    it.printStackTrace()
                }))
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onActivityDestroy() {
        if (!mCompositeDisposable.isDisposed) {
            mCompositeDisposable.dispose()
        }
    }
}